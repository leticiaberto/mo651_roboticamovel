#Alunas:---------------------------------------------------------------------------#
#--------Caroline Mazini Rodrigues-----------------RA 211854-----------------------#
#--------Letícia Berto-----------------------------RA 212069-----------------------#
#--------Sarah Almeida Carneiro--------------------RA 212145-----------------------#

try:
    import vrep
except:
    print ('--------------------------------------------------------------')
    print ('"vrep.py" could not be imported. This means very probably that')
    print ('either "vrep.py" or the remoteApi library could not be found.')
    print ('Make sure both are in the same folder as this file,')
    print ('or appropriately adjust the file "vrep.py"')
    print ('--------------------------------------------------------------')
    print ('')

import time
from matplotlib import pyplot as plt
import cv2
import numpy as np
from ctypes import *
import math
import time
from skimage.measure import regionprops
from skimage.measure import LineModelND, ransac
from math import isclose
import imutils

FLANN_INDEX_KDTREE = 1  # bug: flann enums are missing
FLANN_INDEX_LSH    = 6
MAX_ITERATIONS = 200

#Iniciando conexão################################

print ('Program started')
vrep.simxFinish(-1) # just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',25000,True,True,5000,5) # Connect to V-REP

if clientID!=-1:
    print ('Connected to remote API server')

    # Now try to retrieve data in a blocking fashion (i.e. a service call):
    res,objs=vrep.simxGetObjects(clientID,vrep.sim_handle_all,vrep.simx_opmode_blocking)
    if res==vrep.simx_return_ok:
        print ('Number of objects in the scene: ',len(objs))
    else:
        print ('Remote API function call returned with error code: ',res)

    time.sleep(2)

    
else:
    print ('Failed connecting to remote API server')
    print ('Program ended')
########################################################



#Handles################################################

#robot handle
res,robot =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx',vrep.simx_opmode_oneshot_wait) 

#motors handles
resL,left_motor =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor',vrep.simx_opmode_oneshot_wait) 
resR,right_motor =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor',vrep.simx_opmode_oneshot_wait)

#proximity sensors handles
resS = np.arange(16)
sensor = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]


resS[0],sensor[0] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor1',vrep.simx_opmode_oneshot_wait)
resS[1],sensor[1] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor2',vrep.simx_opmode_oneshot_wait)
resS[2],sensor[2] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor3',vrep.simx_opmode_oneshot_wait)
resS[3],sensor[3] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor4',vrep.simx_opmode_oneshot_wait)
resS[4],sensor[4] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor5',vrep.simx_opmode_oneshot_wait)
resS[5],sensor[5] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor6',vrep.simx_opmode_oneshot_wait)
resS[6],sensor[6] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor7',vrep.simx_opmode_oneshot_wait)
resS[7],sensor[7] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor8',vrep.simx_opmode_oneshot_wait)
resS[8],sensor[8] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor9',vrep.simx_opmode_oneshot_wait)
resS[9],sensor[9] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor10',vrep.simx_opmode_oneshot_wait)
resS[10],sensor[10] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor11',vrep.simx_opmode_oneshot_wait)
resS[11],sensor[11] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor12',vrep.simx_opmode_oneshot_wait)
resS[12],sensor[12] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor13',vrep.simx_opmode_oneshot_wait)
resS[13],sensor[13] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor14',vrep.simx_opmode_oneshot_wait)
resS[14],sensor[14] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor15',vrep.simx_opmode_oneshot_wait)
resS[15],sensor[15] =vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor16',vrep.simx_opmode_oneshot_wait)

#camera handle
res,v0=vrep.simxGetObjectHandle(clientID,'Vision_sensor',vrep.simx_opmode_oneshot_wait) 

#motor handle
motorHandle = [-1,-1]
codeMotor = [-1,-1]
codeEnc = [-1,-1]
encoder = [-1,-1]

codeMotor[0],motorHandle[0]=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor',vrep.simx_opmode_oneshot_wait)
codeMotor[1],motorHandle[1]=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor',vrep.simx_opmode_oneshot_wait)

codeEnc[0],encoder[0] = vrep.simxGetJointPosition(clientID,motorHandle[0],vrep.simx_opmode_streaming)  
codeEnc[1],encoder[1] = vrep.simxGetJointPosition(clientID,motorHandle[1],vrep.simx_opmode_streaming)  

err, laser = vrep.simxGetObjectHandle(clientID,"fastHokuyo",vrep.simx_opmode_oneshot_wait)

laser_x = []
laser_y = []


#############################################################

braitenbergL=[-0.2,-0.4,-0.6,-0.8,-1,-1.2,-1.4,-1.6, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
braitenbergR=[-1.6,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
noDetectionDist=0.5
maxDetectionDist=0.2
time_wait = 0.005
pontos = np.empty([2, 100000])
posicao_vazia = 0
angulos_sonar = [90,50,30,10,-10,-30,-50,-90,-90,-130,-150,-170,170,150,130,90]
linhas = []
lastEncoderPositionR = encoder[0]
lastEncoderPositionL= encoder[1]
clockwiseSpinL = False
clockwiseSpinR = False
lastTime=time.time()

# Movimentacao#############################################

#Move de acordo com velocidade--------------------------  
def robot_move(vLeft,vRight):
    codeL = vrep.simxSetJointTargetVelocity(clientID,left_motor, vLeft,vrep.simx_opmode_oneshot_wait)
    codeR = vrep.simxSetJointTargetVelocity(clientID,right_motor, vRight,vrep.simx_opmode_oneshot_wait)

    return codeL,codeR
#-------------------------------------------------------

#Para robo--------------------------------------------- 
def robot_stop():
	robot_move(0,0)
#-----------------------------------------------------

#Função para movimentar-------------------------------
def robot_drive(vLinear, vAngular):
	return(robot_move(vLToDrive(vLinear,vAngular*math.pi/180),vRToDrive(vLinear,vAngular*math.pi/180)))
#---------------------------------------------------

#Calcula roda direita---------------------------------
def vRToDrive(vLinear, vAngular):
	return (((2*vLinear)+(0.36205*vAngular))/0.195)
#----------------------------------------------------

#Calcula roda esquerda-------------------------------
def vLToDrive(vLinear, vAngular):
	return (((2*vLinear)-(0.36205*vAngular))/0.195)
#----------------------------------------------------

##########################################################

#Odometria############################################

limit = 2*math.pi

#Soma angulos------------------------------------------
def addDelta(start, delta):
    ans = (start + delta) % limit
    if ans > math.pi:
        return ans - limit
    return ans
#---------------------------------------------------


def to180Universe(alpha):
    alpha = alpha % (2 * math.pi)
    if alpha > math.pi:
        return alpha - (2 * math.pi)
    return alpha

def to360Universe(alpha):
    if alpha < 0:
        return (2 * math.pi) + alpha
    return alpha

def addAngles(alpha, beta):
    a = to360Universe(alpha)
    b = to360Universe(beta)
    c = a + b
    return to180Universe(c)


#Encontra nova posicao-------------------------------

def nova_posicao(alphaRobo, x, y):
    global clockwiseSpinL
    global clockwiseSpinR
    global lastTime
    global lastEncoderPositionL
    global lastEncoderPositionR
    actualTime = time.time()
    dtime = (actualTime-lastTime)
    lastTime=actualTime
    R= 0.0975
    L = 0.36205
    speedR,clockwiseSpinR,lastEncoderPositionR = calculateSpeed(dtime, clockwiseSpinR, motorHandle[1], lastEncoderPositionR)
    speedL,clockwiseSpinL,lastEncoderPositionL = calculateSpeed(dtime, clockwiseSpinL, motorHandle[0], lastEncoderPositionL)
    vr = speedR
    vl = speedL
    print('R: '+str(speedR))
    print('L: '+str(speedL))
    w = (vr-vl)/(0.36205)
    v =  (vr+vl)/2
    dAlpha = w*dtime
    dl = vl*dtime
    dr = vr*dtime
    ds = (dl+dr)/2
    dx = ds*math.cos(addAngles(alphaRobo,dAlpha/2))
    dy = ds*math.sin(addAngles(alphaRobo,dAlpha/2))
    return x+dx, y+dy, addAngles(alphaRobo,dAlpha)

#----------------------------------------------------

def calculateDelta(start, end, spinRightOrientation):
    if isclose(end, start, abs_tol=0.00001):
        return 0
    
    end = (limit + end) % limit
    start = (limit + start) % limit

    ans = abs(end - start)
    if spinRightOrientation:
        if start < end:
            return limit - ans
        return ans
    if start > end:
        return limit - ans
    return ans


def calculateSpeed(timeDelta, clockwiseSpin, motorHandle, lastEncoderPosition):
    delta,clockwiseSpin,lastEncoderPosition = getDeltaAngle(motorHandle, lastEncoderPosition,clockwiseSpin)

    speed = 0.0975 * (delta / timeDelta)

    if clockwiseSpin:
        return -speed,clockwiseSpin, lastEncoderPosition
    return speed,clockwiseSpin,lastEncoderPosition



def getDeltaAngle(motorHandle, lastEncoderPosition, clockwiseSpin):
    res,encoder = vrep.simxGetJointPosition(clientID,motorHandle,vrep.simx_opmode_streaming)
    delta = calculateDelta(lastEncoderPosition, encoder, clockwiseSpin)

    lastEncoderPosition = encoder
    # Hack: spin orientation change
    if delta > math.pi:
        delta = (2 * math.pi) - delta
        clockwiseSpin = not clockwiseSpin

    return delta,clockwiseSpin, lastEncoderPosition




#######################################################

#Nuvem pontos###########################################

#Salva vetor de pontos----------------------

def salva_vetor_pontos(pontos, nome):

    np.save(nome + ".npy", pontos)

#------------------------------------------

#Plota gráfico com pontos sonar---------------------
def plota_pontos(pontos_convertidos, nome_save, legenda):

    fig, ax = plt.subplots(figsize=(8, 8))
    ax.scatter(-pontos_convertidos[1,:posicao_vazia],pontos_convertidos[0,:posicao_vazia],c='r',marker='.')
    ax.grid(True)
    #ax.legend(loc='best')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(legenda)
    plt.savefig(nome_save+'.png') 
    plt.show()
#--------------------------------------------

#Ler e converter pontos sonar ----------------------
 
def pontos_por_leitura(angulo_robo, coord_objetos, presenca_objetos, coord_robo):
    global posicao_vazia
    for i in range(0,16):
        if (presenca_objetos[i] == True):
            angulo = addDelta(angulo_robo,math.radians(angulos_sonar[i]))
            pontos[0,posicao_vazia] = (coord_robo[0]+((coord_objetos[i]+0.0975)*math.cos(angulo)))
            pontos[1,posicao_vazia] = (coord_robo[1]+((coord_objetos[i]+0.0975)*math.sin(angulo)))
            posicao_vazia = posicao_vazia+1

#-----------------------------------------------

#######################################################

#Extrai linhas do mapa sonar################################

def incremental(maxVariance, ini, maxDist):
    line = np.empty(4)
    pontos_buffer = []
    pontos_aux = np.copy(np.copy(pontos[:,ini:posicao_vazia]))
    index,menorDist = orderPointsDistance(0,pontos_aux)

    if(menorDist>maxDist):
        pontos_aux[:,0] = pontos_aux[:,index]
        pontos_aux = np.delete(pontos_aux,index,1)

    line[0] = np.copy(pontos_aux[0,0])
    line[1] = np.copy(pontos_aux[1,0])
    line[2] = np.copy(pontos_aux[0,index])
    line[3] = np.copy(pontos_aux[1,index]) 
    pontos_buffer.append(np.copy(pontos_aux[:,0]))
    pontos_buffer.append(np.copy(pontos_aux[:,index]))

    pontos_aux[:,0] = pontos_aux[:,index]
    pontos_aux = np.delete(pontos_aux,index,1)

    coefAngAnt = (line[3]-line[1])/(line[2]-line[0]+0.000001)
    coefCAnt = line[0]*line[3] - line[1] - line[2]
    while (len(pontos_aux[0,:])>2):
        #encontrar a equação da reta entre o primeiro de line e o i
        index,menorDist = orderPointsDistance(0,pontos_aux)

        if(menorDist>maxDist):
            pontos_aux[:,0] = pontos_aux[:,index]
            pontos_aux = np.delete(pontos_aux,index,1)
        else:
            coefAng = (pontos_aux[1,index]-line[1])/(pontos_aux[0,index]-line[0]+0.000001)
            coefC = line[0]*pontos_aux[1,index] - line[1] - pontos_aux[0,index]
            length = len(pontos_buffer)
            print(length)
            var_pontos = np.empty(length)
            for i in range(0,len(pontos_buffer)):
                var_pontos[i] = distPointLine(coefAng,coefC,pontos_buffer[i][0],pontos_buffer[i][1])

            variancia= np.var(var_pontos)
            #calcular variancias dos pontos em pontos_buffer para os parametros da reta
            if(maxVariance<variancia):
                if(len(pontos_buffer)>5):
                    linhas.append(np.copy(line))
                pontos_aux[:,0] = pontos_aux[:,index]
                pontos_aux = np.delete(pontos_aux,index,1)
                line[0] = np.copy(pontos_aux[0,0])
                line[1] = np.copy(pontos_aux[1,0])
                index,menorDist = orderPointsDistance(0,pontos_aux)
                while(menorDist>maxDist and len(pontos_aux[0,:])>2):
                    pontos_aux[:,0] = pontos_aux[:,index]
                    pontos_aux = np.delete(pontos_aux,index,1)
                    line[0] = np.copy(pontos_aux[0,0])
                    line[1] = np.copy(pontos_aux[1,0])
                    index,menorDist = orderPointsDistance(0,pontos_aux)
                if(len(pontos_aux[0,:])<=2):
                    break
                line[2] = np.copy(pontos_aux[0,index])
                line[3] = np.copy(pontos_aux[1,index])
                pontos_buffer = []
                pontos_buffer.append(np.copy(pontos_aux[:,0]))
                pontos_buffer.append(np.copy(pontos_aux[:,index]))
                coefAng = (line[3]-line[1])/(line[2]-line[0]+0.000001)
                coefC = line[0]*line[3] - line[1] - line[2]
            else:
                line[2] = np.copy(pontos_aux[0,index])
                line[3] = np.copy(pontos_aux[1,index]) 
                pontos_buffer.append(np.copy(pontos_aux[:,index]))
        
            pontos_aux[:,0] = pontos_aux[:,index]
            pontos_aux = np.delete(pontos_aux,index,1)

            coefAngAnt = coefAng
            coefCAnt = coefC
    if(len(pontos_buffer)>5):
        linhas.append(np.copy(line))
    return posicao_vazia

def distPointLine(coefAng, coefC, x, y):
    return(abs(coefAng*x-y+coefC)/math.sqrt(x*x+y*y))

def distance(p1, p2):
     return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)

def orderPointsDistance(ini,pontos):
    distances = []
    for i in range(ini+1,len(pontos[0,:])):
        distances.append(distance(pontos[:,ini],pontos[:,i]))
    index = np.argmin(distances)
    return index, distances[index]

        

#####################################################


#Plota gráfico com pontos laser---------------------
def plota_pontosLaser(pontosLaserX, pontosLaserY, nome_save, legenda):

    fig, ax = plt.subplots(figsize=(8, 8))
    ax.scatter(pontosLaserX,pontosLaserY,c='r',marker='.')
    ax.grid(True)
    ax.legend(loc='best')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title(legenda)
    plt.savefig(nome_save+'.png')
    plt.show()

#Ronny
def convAngulo(alpha):
    return (alpha+2*math.pi)%(2*math.pi)

#Ronny
def tranforma_global(xLocal, yLocal, x, y, alpha):

   T_trans = [[1, 0, x],
                [0, 1, y],
                 [0, 0, 1]]
   T_rot = [[math.cos(alpha), -math.sin(alpha), 0],
               [math.sin(alpha), math.cos(alpha),0],
               [0,0,1]]
   T = np.matmul(T_trans, T_rot)
   
   pos = np.matmul(T, np.transpose([xLocal,yLocal,1.0]))

   return pos

def pontos_por_leituraLaser(angulo_robo, coord_robo, lrf):
    global posicao_vaziaLaser
    i = 0
    while(i < len(lrf)-2):
        ponto_ = tranforma_global(lrf[i], lrf[i+1], coord_robo[0], coord_robo[1], convAngulo(angulo_robo))
        laser_x.append(-ponto_[1])
        laser_y.append(ponto_[0])
        i = i+3



#####################################################



#Encontra porta########################################

#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# Finds door based on its pre-defined color
#------------------------------------------------------------------------------
def find_door (img):
    #img = cv2.imread(frame)
    ## convert to hsv
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    ## mask of green 
    mask = cv2.inRange(hsv, (110, 150, 50), (120, 255,255))

    ## slice the green
    imask = mask>0
    color = np.zeros_like(img, np.uint8)
    color[imask] = img[imask]
    ##show
    cv2.imshow("Door", color)
    ## save 
    cv2.imwrite("possibleDoor.png", color)
    ## return
    x,y = find_door_centroid (color)
    return x,y
#------------------------------------------------------------------------------
# Find doors centroid so robot can follow
#------------------------------------------------------------------------------

def find_door_centroid (img):

    # convert it to grayscale, blur it slightly, and threshold it
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 5, 255, cv2.THRESH_BINARY)[1]
    # find contours in the thresholded img
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    # loop over the contours
    for c in cnts:
        try:
                # compute the center of the contour
                M = cv2.moments(c)
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                # draw the contour and center of the shape on the img
                cv2.drawContours(img, [c], -1, (0, 255, 0), 2)
                cv2.circle(img, (cX, cY), 7, (255, 255, 255), -1)
                cv2.putText(img, "Follow", (cX - 20, cY - 20), 
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
                # show the img
                cv2.imshow("img", img)
                cv2.imwrite("centroid.png", img)
                return cX, cY
        except:
                print ("not able to calculate centroid")

########################################################

#Loop de controle######################################

fig3, ax3 = plt.subplots(figsize=(8, 8))
err, dataDist = vrep.simxGetStringSignal(clientID,"Laser",vrep.simx_opmode_streaming)


r,coord = vrep.simxGetObjectPosition(clientID,robot,vrep.sim_handle_parent,vrep.simx_opmode_streaming)
r2,orient = vrep.simxGetObjectOrientation(clientID,robot,vrep.sim_handle_parent,vrep.simx_opmode_streaming)

alpha_inicial = orient[2]
x_incial = coord[0]
y_inicial = coord[1]

iteration = 0
x=coord[0]
y=coord[1]
alphaRobo = orient[2]
trajetoriaOdometria = np.empty([MAX_ITERATIONS,2])
trajetoria = np.empty([MAX_ITERATIONS,2])
j=0
nextPoint = 0

err, resolution, image = vrep.simxGetVisionSensorImage(clientID, v0, 0, vrep.simx_opmode_streaming)
while(iteration<MAX_ITERATIONS):
    vRight = 3
    vLeft = 3
    iteration = iteration+1
    detect=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    dist=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    detect2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    detectionState = [False, False,False,False,False, False,False,False,False, False,False,False,False, False,False,False]
    arr1 = np.empty([16,3])   


    #leitura sonares------------------------
   
    coord2 = [0,0,0]

    for i in range(0,16):
        dist[i],detectionState[i], arr1[i,:], detectedObjectHandle, arr2=vrep.simxReadProximitySensor(clientID, sensor[i], vrep.simx_opmode_oneshot)
        if (detectionState[i]==True and dist[i]<noDetectionDist):
            if (dist[i]<maxDetectionDist):
                dist[i]=maxDetectionDist
            detect[i]=1-((dist[i]-maxDetectionDist)/(noDetectionDist-maxDetectionDist))
            detect2[i] = 1
            
        else:
            detect[i]=0
    
    pontos_por_leitura(orient[2], dist, detectionState, coord)
    

    #Laser----------------------------------------
    err, data = vrep.simxGetStringSignal(clientID,"Laser",vrep.simx_opmode_buffer)
    if (err == vrep.simx_return_ok):
        distance_laser = vrep.simxUnpackFloats(data)
        pontos_por_leituraLaser(orient[2], coord, distance_laser)


    #------------------------------------------   

    #movimento--------------------------------- 
    for i in range(0,16):
        vLeft=vLeft+braitenbergL[i]*detect[i]
        vRight=vRight+braitenbergR[i]*detect[i]

    codeL = vrep.simxSetJointTargetVelocity(clientID, left_motor,vLeft,vrep.simx_opmode_blocking)
    codeR = vrep.simxSetJointTargetVelocity(clientID, right_motor,vRight,vrep.simx_opmode_blocking)

    while(codeL!=0 or codeR!=0):
        codeL = vrep.simxSetJointTargetVelocity(clientID, left_motor,vLeft,vrep.simx_opmode_blocking)
        codeR = vrep.simxSetJointTargetVelocity(clientID, right_motor,vRight,vrep.simx_opmode_blocking)

    #-----------------------------------------
    
    
    
    #camera image-------------------- 
    match = False
    entrou = False
    y1=0
    x1=0
    err, resolution, image = vrep.simxGetVisionSensorImage(clientID, v0, 0, vrep.simx_opmode_streaming)
    if err == vrep.simx_return_ok:
        print("image OK!!!")
        img = np.array(image,dtype=np.uint8)
        img.resize([resolution[1],resolution[0],3])
        img2 = cv2.flip(img, 0)
        meioX = resolution[1]//2
        meioY = resolution[0]//2
        
        try:
            x1, y1 = find_door (img2)
            print ("X: " + str(x1))
            print ("Y: " + str(y1))
            match=True
            robot_stop()
            time.sleep(1)
        except:
            match = False
            print ("No door in frame")

        print(match)
        
        #Enquanto continuar vendo a porta--------------------------
        while(match==True):
            dist[3],detectionState[3], arr1[3,:], detectedObjectHandle, arr2=vrep.simxReadProximitySensor(clientID, sensor[3], vrep.simx_opmode_oneshot)
            dist[4],detectionState[4], arr1[4,:], detectedObjectHandle, arr2=vrep.simxReadProximitySensor(clientID, sensor[4], vrep.simx_opmode_oneshot)
            dist[2],detectionState[2], arr1[2,:], detectedObjectHandle, arr2=vrep.simxReadProximitySensor(clientID, sensor[2], vrep.simx_opmode_oneshot)
            dist[5],detectionState[5], arr1[5,:], detectedObjectHandle, arr2=vrep.simxReadProximitySensor(clientID, sensor[5], vrep.simx_opmode_oneshot)
            
            #se estiver prestes a colidir com a porta---------------
            if((detectionState[3] and dist[3]<0.3) or (detectionState[4] and dist[4]<0.3) or (detectionState[2] and dist[2]<0.3)or (detectionState[5] and dist[5]<0.3)):
                robot_drive(0,-10)
                time.sleep(0.5)

                while(x1>70):
                    robot_drive(0.5,-10)
                    time.sleep(0.5)
                    robot_drive(0.05,0)

                    err, resolution, image = vrep.simxGetVisionSensorImage(clientID, v0, 0, vrep.simx_opmode_buffer)
                    if err == vrep.simx_return_ok:
                        print("image OK!!!")
                        img = np.array(image,dtype=np.uint8)
                        img.resize([resolution[1],resolution[0],3])
                        img2 = cv2.flip(img, 0)

                        cv2.imshow('image',img2)
                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            break
                    elif err == vrep.simx_return_novalue_flag:
                        print("no image yet")
                        pass
            
                    try:
                        x1, y1 = find_door (img2)
                        print ("X: " + str(x1))
                        print ("Y: " + str(y1))
                        match=True
                        robot_stop()
                        time.sleep(1)
                    except:
                        match = False
                        print ("No door in frame")
                    
                    if(x1<60):
                        robot_drive(0,10)
                        time.sleep(0.05) 
                        robot_drive(0.05,0)


                robot_drive(0.5,5)
                time.sleep(2)
                print('Entrouuuu')
                entrou = True
                break
                print('Virou para direita por obstrução') 


            #Se estiver muito à esquerda, centralizar centroid-----------
            elif(x1>=meioX):
                robot_drive(0,-10)
                robot_drive(0.5,0)
                time.sleep(0.05)
                robot_stop()
                print('Virou para direita')
            #Se estiver muito à direita, centralizar centroid------------
            elif(x1<(meioX)):
                robot_drive(0,10)
                robot_drive(0.5,0)
                time.sleep(0.05)
                robot_stop()
                print('Virou para esquerda')
            #Senão, vira um pouco pra direeita
            else:
                robot_drive(0,-10)
                robot_drive(0.5,0)
                time.sleep(0.05)
                robot_stop()
                print('Virou para direita')
            time.sleep(1)

            err, resolution, image = vrep.simxGetVisionSensorImage(clientID, v0, 0, vrep.simx_opmode_buffer)
            if err == vrep.simx_return_ok:
                print("image OK!!!")
                img = np.array(image,dtype=np.uint8)
                img.resize([resolution[1],resolution[0],3])
                img2 = cv2.flip(img, 0)
                cv2.imshow('image',img2)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            elif err == vrep.simx_return_novalue_flag:
                print("no image yet")
            pass
            
            try:
                x1, y1 = find_door (img2)
                print ("X: " + str(x1))
                print ("Y: " + str(y1))
                match=True
                robot_stop()
                time.sleep(1)
            except:
                match = False
                print ("No door in frame")

        #Se entrar, para, lê laser e sai de ré------------------
        if entrou:
            robot_stop()
            #Laser----------------------------------------
            err, data = vrep.simxGetStringSignal(clientID,"Laser",vrep.simx_opmode_buffer)
            if (err == vrep.simx_return_ok):
                distance_laser = vrep.simxUnpackFloats(data)
                pontos_por_leituraLaser(orient[2], coord, distance_laser)
            time.sleep(5)
            robot_drive(0,10)
            time.sleep(0.05)
            robot_drive(-0.5,0)
            time.sleep(5)
            robot_drive(0,90)
            time.sleep(1)
         
        cv2.imshow('image',img2)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    elif err == vrep.simx_return_novalue_flag:
        print("no image yet")
        pass
    
    #-------------------------------------

    r,coord = vrep.simxGetObjectPosition(clientID,robot,vrep.sim_handle_parent,vrep.simx_opmode_buffer)
    r2,orient = vrep.simxGetObjectOrientation(clientID,robot,vrep.sim_handle_parent,vrep.simx_opmode_buffer)

    #Calcula a odometria---------------------
    #Quanto a parte da camera está ativada o movimento para seguir a porta não é registrado na odometria
    x,y,alphaRobo = nova_posicao(alphaRobo, x, y)

    trajetoriaOdometria[iteration-1,0] = -y
    trajetoriaOdometria[iteration-1,1] = x
    trajetoria[iteration-1,0] = -coord[1]
    trajetoria[iteration-1,1] = coord[0]

    #---------------------------------------

    #Calcula linhas a partir de 10 leituras consecutivas do sonar
    if(posicao_vazia>nextPoint+1):
        if(j%10==0):
            nextPoint = incremental(0.8,nextPoint,0.5)
        j=j+1

    #--------------------------------------
    
    time.sleep(time_wait)
    


#para critério visual, somente para transladar a odometria que iniciou-se no ponto 0,0 com orientação 0
# para o x,y inicial do ground truth, considerando o robo na orientação inicial 0 (sem realizar rotação portanto)
difx = trajetoria[1,0] - trajetoriaOdometria[1,0]
dify = trajetoria[1,1] - trajetoriaOdometria[1,1]

trajetoriaOdometria[:,0] = trajetoriaOdometria[:,0]+difx
trajetoriaOdometria[:,1] = trajetoriaOdometria[:,1]+dify

#-----------------------------------------------------------

#Plota odometria e ground truth--------------------------
ax3.plot(trajetoriaOdometria[:,0], trajetoriaOdometria[:,1], label = 'Odometry')
ax3.plot(trajetoria[:,0], trajetoria[:,1], label = 'Ground Truth')
ax3.grid(True)
ax3.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.title('Odometry x Ground Truth')
plt.savefig('odometry'+'.png') 
plt.show()
#----------------------------------------------------

#Plota pontos sonar e laser---------------------------
plota_pontos(pontos, 'teste', 'Sonar Map')

plota_pontosLaser(laser_x, laser_y, 'laserMap', 'Laser Map')

#----------------------------------------------------

#Plota linhas obtidas do sonar----------------------
fig2, ax2 = plt.subplots(figsize=(8, 8))

for i in range(0,len(linhas)):
    ax2.plot([-linhas[i][1],-linhas[i][3]], [linhas[i][0],linhas[i][2]])
plt.xlabel("x")
plt.ylabel("y")
plt.title('Lines Extracted')
plt.savefig('lines'+'.png') 
plt.show()

while(len(linhas[:][0])>0):
    linhaAtual = np.copy(linhas[0][:])
    linhas = np.delete(linhas,0,1)




#########################################################


#Encerra conexão#######################################


# Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
vrep.simxGetPingTime(clientID)

# Now close the connection to V-REP:
vrep.simxFinish(clientID)

#########################################################